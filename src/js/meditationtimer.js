var counter;
var variableTiming = false;
var totalSecondsToCount = 0;
var delayIsComplete = false;
var secondsCounted = 0;
var pathToSoundFile = "/resources/sounds/bowl.mp3";

function initiate() {
    hideInputErrorAlert();
    if (!validateInput()) {
        return;
    }
    delayTime = +$("#startDelayInput").val();
    if (!variableTiming) {
        startCountdownTimer(delayTime, +$("#minutesMinimumInput").val());
    } else {
        startCountdownTimer(delayTime, +$("#minutesMinimumInput").val(), +$("#minutesMaximumInput").val());
    }
    setControlsEnabled(false);
}

function validateInput() {
    if ($("#startDelayInput").val() == "") {
        showInputErrorAlert("Please ensure input values for time are valid numbers.");
        return false;
    }
    if ($("#minutesMinimumInput").val() == "") {
        showInputErrorAlert("Please ensure input values for time are valid numbers.");
        return false;
    }
    if (variableTiming) {
        if ($("#minutesMaximumInput").val() == "") {
            showInputErrorAlert("Please ensure input values for time are valid numbers.");
            return false;
        }
        if (+$("#minutesMinimumInput").val() > +$("#minutesMaximumInput").val() || +$("#minutesMaximumInput").val() < +$("#minutesMinimumInput").val()) {
            showInputErrorAlert("Please ensure minimum time is less than maximum time.");
            return false;
        }
    }
    if (+$("#minutesMinimumInput").val() > 120 || +$("#minutesMaximumInput").val() > 120) {
        showInputErrorAlert("Please limit maximum meditation time to 120 minutes.");
            return false;
    }
    if (+$("#startDelayInput").val() > 60) {
        showInputErrorAlert("Please limit maximum delay to 60 seconds.");
            return false;
    }
    return true;
}

function startCountdownTimer(delaySeconds, minutesMinimum, minutesMaximum) {
    stopTimer();
    secondsCounted = 0;
    if (minutesMaximum) {
        minutes = parseInt(Math.random() * ((minutesMaximum + 1) - minutesMinimum) + minutesMinimum);
    } else {
        minutes = minutesMinimum;
    }
    totalSecondsToCount = ((minutes * 60) + delaySeconds + 1);
    console.log("Starting timer for " + minutes + " minutes.")
    $("#progressBar").width("100%");
    if (delaySeconds > 0) {
        showSecondsDelayAlert(delaySeconds);
    }
    delayIsComplete = false;
    counter = setInterval(function () {
        tickTimer(delaySeconds);
    }, 1000);
}

function tickTimer(delaySeconds) {
    secondsCounted = secondsCounted + 1;
    if (delayIsComplete) {
        var percentRemaining = 100 - ((secondsCounted - delaySeconds) / (totalSecondsToCount - delaySeconds)) * 100;
        $("#progressBar").width(percentRemaining + "%");
        if (secondsCounted == totalSecondsToCount) {
            clearInterval(counter);
            playSound(pathToSoundFile);
            setControlsEnabled(true);
        }
        return;
    } else if (delaySeconds == 0 || secondsCounted == delaySeconds) {
        delayIsComplete = true;
        hideSecondsDelayAlert();
        playSound(pathToSoundFile);
        return;
    }
    if (secondsCounted < delaySeconds) {
        $("#delayAlertSecondsDisplay").text(delaySeconds - secondsCounted);
        return;
    }
}

function playSound(soundFile) {
    let audio = new Audio(soundFile);
    audio.play();
}

function showSecondsDelayAlert(seconds) {
    $("#delayAlertSecondsDisplay").text(seconds);
    $('#secondsDelayAlert').show();
}

function hideSecondsDelayAlert() {
    $('#secondsDelayAlert').fadeOut();
}

function showInputErrorAlert(text) {
    $("#inputErrorAlert").text(text);
    $('#inputErrorAlert').show();
}

function hideInputErrorAlert() {
    $('#inputErrorAlert').hide();
}

function stopTimer() {
    hideSecondsDelayAlert();
    clearInterval(counter);
    $("#progressBar").width("0%");
    setControlsEnabled(true);
}

$('#timingModeSelectRadioButtons :input').change(function () {
    if ($('#timingModeSelectRadioButtons input:radio:checked').val() == "fixed") {
        variableTiming = false;
        $("#minutesInput").html("Minutes: <input type=\"number\" id=\"minutesMinimumInput\" name=\"minutesMinimumInput\" min=\"1\" max=\"60\" value=\"5\">");
    } else {
        variableTiming = true;
        $("#minutesInput").html("Between: <input type=\"number\" id=\"minutesMinimumInput\" name=\"minutesMinimumInput\" min=\"1\" max=\"59\" value=\"5\"> and <input type=\"number\" id=\"minutesMaximumInput\" name=\"minutesMaximumInput\" min=\"1\" max=\"60\" value=\"10\"> minutes");
    }
});

$('#soundSelectRadioButtons :input').change(function () {
    if ($('#soundSelectRadioButtons input:radio:checked').val() == "bowl") {
        pathToSoundFile = "/resources/sounds/bowl.mp3"
        playSound(pathToSoundFile);

    } else if ($('#soundSelectRadioButtons input:radio:checked').val() == "chime") {
        pathToSoundFile = "/resources/sounds/chime.mp3"
        playSound(pathToSoundFile);
    }
    else if ($('#soundSelectRadioButtons input:radio:checked').val() == "majestic_gong") {
        pathToSoundFile = "/resources/sounds/majestic_gong.mp3"
        playSound(pathToSoundFile);
    }
    else if ($('#soundSelectRadioButtons input:radio:checked').val() == "burmese_temple_gong") {
        pathToSoundFile = "/resources/sounds/burmese_temple_gong.mp3"
        playSound(pathToSoundFile);
    } else if ($('#soundSelectRadioButtons input:radio:checked').val() == "chinese_gong") {
        pathToSoundFile = "/resources/sounds/chinese_gong.mp3"
        playSound(pathToSoundFile);
    }
});

function setControlsEnabled(state) {
    if (state == false) {
        $("#stopButton").prop('disabled', false);
        $("#startButton").prop('disabled', true);
        $('#soundSelectRadioButtons :input').prop('disabled', true);
        $('#timingModeSelectRadioButtons :input').prop('disabled', true);
        $('#minutesMinimumInput').prop('disabled', true);
        if ($('#minutesMaximumInput')) {
            $('#minutesMaximumInput').prop('disabled', true);
        }
        $('#startDelayInput').prop('disabled', true);
    } else {
        $("#stopButton").prop('disabled', true);
        $("#startButton").prop('disabled', false);
        $('#soundSelectRadioButtons :input').prop('disabled', false);
        $('#timingModeSelectRadioButtons :input').prop('disabled', false);
        $('#minutesMinimumInput').prop('disabled', false);
        if ($('#minutesMaximumInput')) {
            $('#minutesMaximumInput').prop('disabled', false);
        }
        $('#startDelayInput').prop('disabled', false);
    }
}

$("#startButton").click(initiate);
$("#stopButton").click(stopTimer);