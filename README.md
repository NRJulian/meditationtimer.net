# MeditationTimer.net

 MeditationTimer.net is a web app for timing meditation sessions, built on [Bootstrap 5](https://getbootstrap.com/docs/5.0/getting-started/introduction) and [JQuery](https://jquery.com/).
 
![screenshot_1](./images/screenshot1.png)

 Images are adapted from public domain files available at [freesvg.org](https://freesvg.org/). 
 
 Sound effects are taken from CC0 public domain resources from [freesound.org](https://freesound.org/) as well as audio recorded using the free [Healing-Lite VST](https://www.kvraudio.com/product/healing-lite-by-quiet-music).

MeditationTimer.net is free and open-source software under the [MIT](https://choosealicense.com/licenses/mit/) license.
